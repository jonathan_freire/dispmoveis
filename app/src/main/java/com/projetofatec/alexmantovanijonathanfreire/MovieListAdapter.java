package com.projetofatec.alexmantovanijonathanfreire;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jonathan on 25/07/2016.
 */
public class MovieListAdapter extends ArrayAdapter<MovieListItem> {
    public static class ViewHolder {
        ImageView imageViewMovie;
        TextView textViewMovieName;
        TextView textViewReleaseDate;
    }
    public Map<String, Bitmap> images = new HashMap<>();
    public MovieListAdapter(Context context, List<MovieListItem> objects) {
        super(context, -1, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        MovieListItem item = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.list_item, parent, false);
            viewHolder.imageViewMovie = (ImageView) convertView.findViewById(R.id.imageViewMovie);
            viewHolder.textViewMovieName = (TextView) convertView.findViewById(R.id.textViewMovieName);
            viewHolder.textViewReleaseDate = (TextView) convertView.findViewById(R.id.textViewReleaseDate);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (images.containsKey(item.getImageUrl())) {
            viewHolder.imageViewMovie.setImageBitmap(images.get(item.getImageUrl()));
        } else {
            new DownloadImageTask(viewHolder.imageViewMovie).execute(item.getImageUrl());
        }
        viewHolder.textViewMovieName.setText(item.getMovieName());
        viewHolder.textViewReleaseDate.setText (this.getFormatedDate(item.getPublicationDate()));//Formata Data

        return convertView;
    }
    private String getFormatedDate(String PublicationDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(PublicationDate);
        } catch (ParseException e) {
            return PublicationDate;
        }
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getContext());
        String formatedDate = dateFormat.format(date);
        return formatedDate;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView imgView;

        public DownloadImageTask(ImageView imageView) {
            this.imgView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            HttpURLConnection connection = null;
            if(!strings[0].equals("")) {
                try {
                    URL url = new URL(strings[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    try (InputStream inputStream = connection.getInputStream()) {
                        bitmap = BitmapFactory.decodeStream(inputStream);
                        images.put(strings[0], bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if(bitmap != null) {
                imgView.setImageBitmap(bitmap);
            }
        }
    }
}
