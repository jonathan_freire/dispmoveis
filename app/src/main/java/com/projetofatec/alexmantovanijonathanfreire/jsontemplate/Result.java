package com.projetofatec.alexmantovanijonathanfreire.jsontemplate;

public class Result
{
    private String display_title;

    public String getDisplayTitle() { return this.display_title; }

    public void setDisplayTitle(String display_title) { this.display_title = display_title; }

    private String mpaa_rating;

    public String getMpaaRating() { return this.mpaa_rating; }

    public void setMpaaRating(String mpaa_rating) { this.mpaa_rating = mpaa_rating; }

    private int critics_pick;

    public int getCriticsPick() { return this.critics_pick; }

    public void setCriticsPick(int critics_pick) { this.critics_pick = critics_pick; }

    private String byline;

    public String getByline() { return this.byline; }

    public void setByline(String byline) { this.byline = byline; }

    private String headline;

    public String getHeadline() { return this.headline; }

    public void setHeadline(String headline) { this.headline = headline; }

    private String summary_short;

    public String getSummaryShort() { return this.summary_short; }

    public void setSummaryShort(String summary_short) { this.summary_short = summary_short; }

    private String publication_date;

    public String getPublicationDate() { return this.publication_date; }

    public void setPublicationDate(String publication_date) { this.publication_date = publication_date; }

    private String opening_date;

    public String getOpeningDate() { return this.opening_date; }

    public void setOpeningDate(String opening_date) { this.opening_date = opening_date; }

    private String date_updated;

    public String getDateUpdated() { return this.date_updated; }

    public void setDateUpdated(String date_updated) { this.date_updated = date_updated; }

    private Link link;

    public Link getLink() { return this.link; }

    public void setLink(Link link) { this.link = link; }

    private Multimedia multimedia;

    public Multimedia getMultimedia() { return this.multimedia; }

    public void setMultimedia(Multimedia multimedia) { this.multimedia = multimedia; }
}

