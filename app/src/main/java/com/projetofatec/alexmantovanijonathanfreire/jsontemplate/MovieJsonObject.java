package com.projetofatec.alexmantovanijonathanfreire.jsontemplate;

import java.util.ArrayList;

public class MovieJsonObject
{
    private String status;

    public String getStatus() { return this.status; }

    public void setStatus(String status) { this.status = status; }

    private String copyright;

    public String getCopyright() { return this.copyright; }

    public void setCopyright(String copyright) { this.copyright = copyright; }

    private boolean has_more;

    public boolean getHasMore() { return this.has_more; }

    public void setHasMore(boolean has_more) { this.has_more = has_more; }

    private int num_results;

    public int getNumResults() { return this.num_results; }

    public void setNumResults(int num_results) { this.num_results = num_results; }

    private ArrayList<Result> results;

    public ArrayList<Result> getResults() { return this.results; }

    public void setResults(ArrayList<Result> results) { this.results = results; }
}
