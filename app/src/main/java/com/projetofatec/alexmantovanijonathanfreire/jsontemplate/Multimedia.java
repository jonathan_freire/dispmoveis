package com.projetofatec.alexmantovanijonathanfreire.jsontemplate;

public class Multimedia
{
    private String type;

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    private String src;

    public String getSrc() { return this.src; }

    public void setSrc(String src) { this.src = src; }

    private int width;

    public int getWidth() { return this.width; }

    public void setWidth(int width) { this.width = width; }

    private int height;

    public int getHeight() { return this.height; }

    public void setHeight(int height) { this.height = height; }
}
