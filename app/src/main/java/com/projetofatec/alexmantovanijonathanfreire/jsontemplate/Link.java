package com.projetofatec.alexmantovanijonathanfreire.jsontemplate;

public class Link
{
    private String type;

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    private String url;

    public String getUrl() { return this.url; }

    public void setUrl(String url) { this.url = url; }

    private String suggested_link_text;

    public String getSuggestedLinkText() { return this.suggested_link_text; }

    public void setSuggestedLinkText(String suggested_link_text) { this.suggested_link_text = suggested_link_text; }
}
