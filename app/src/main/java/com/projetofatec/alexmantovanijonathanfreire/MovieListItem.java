package com.projetofatec.alexmantovanijonathanfreire;

import android.content.Context;

import com.projetofatec.alexmantovanijonathanfreire.jsontemplate.Result;

import java.text.DateFormat;
import java.text.ParseException;

/**
 * Created by Jonathan on 25/07/2016.
 */
public class MovieListItem {
    private String movieName;
    public String getMovieName() { return this.movieName; }
    public void setMovieName(String MovieName) { this.movieName = MovieName; }

    private String publicationDate;
    public String getPublicationDate() { return this.publicationDate; }
    public void setPublicationDate(String PublicationDate) { this.publicationDate = PublicationDate; }

    private String imgUrl;
    public String getImageUrl() { return this.imgUrl; }
    public void setImageUrl(String ImageUrl) { this.imgUrl = ImageUrl; }

    private Result movieResult;
    public Result getMovieResult() { return this.movieResult; }
    public void setMovieResult(Result MovieResult) { this.movieResult = MovieResult; }

    public MovieListItem(Context context,Result MovieResult){
        this.movieResult = MovieResult;
        this.movieName = MovieResult.getDisplayTitle();
        this.publicationDate = MovieResult.getPublicationDate();
        if(MovieResult.getMultimedia()!=null) {
            this.imgUrl = MovieResult.getMultimedia().getSrc();
        }else
        {
            this.imgUrl= context.getResources().getString(R.string.default_image_url);
        }
    }
}
