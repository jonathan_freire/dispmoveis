package com.projetofatec.alexmantovanijonathanfreire;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.projetofatec.alexmantovanijonathanfreire.jsontemplate.MovieJsonObject;
import com.projetofatec.alexmantovanijonathanfreire.jsontemplate.Result;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton fab;
    EditText editTextMovieName;
    ProgressBar progress;
    private List<MovieListItem> moviesList = new ArrayList<>();
    private MovieListAdapter moviesArrayAdapter;
    private ListView listViewReviews;
    public static MovieListItem SelectedItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        editTextMovieName = (EditText) findViewById(R.id.editTextMovieName);
        listViewReviews = (ListView) findViewById(R.id.listViewReviews);
        moviesArrayAdapter = new MovieListAdapter(this, moviesList);
        progress=(ProgressBar)findViewById(R.id.progress);
        listViewReviews.setAdapter(moviesArrayAdapter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateList();
            }
        });
        listViewReviews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MovieListAdapter adapter = (MovieListAdapter) adapterView.getAdapter();
                SelectedItem = adapter.getItem(i);
                Intent reviewIntent = new Intent(view.getContext(),ReviewActivity.class);

                MovieListAdapter.ViewHolder viewHolder = (MovieListAdapter.ViewHolder)view.getTag();
                reviewIntent.putExtra("BitmapImage", adapter.images.get(SelectedItem.getImageUrl()));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) view.getContext(), viewHolder.imageViewMovie, getString(R.string.image_transitionName));
                    startActivity(reviewIntent, options.toBundle());
                }
                else {
                    startActivity(reviewIntent);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void UpdateList() {
        String movieToSearch = this.editTextMovieName.getText().toString();
        progress.setVisibility(View.VISIBLE);
        dismissKeyboard(this.editTextMovieName);
        new GetMovieTask().execute(createURL(movieToSearch));
    }
    private void dismissKeyboard (View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(),0);
    }
    private URL createURL(String Movie) {
        //https://api.nytimes.com/svc/movies/v2/reviews/search.json?api_key=sua_chave_aqui&q=filme
        String apiKey = getString(R.string.nyt_api_key);
        String apiUrl = getString(R.string.nyt_api_url);
        try {
            String urlString = apiUrl + "?api_key=" + apiKey + "&query=" + URLEncoder.encode(Movie, "UTF-8");
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class GetMovieTask extends AsyncTask<URL, Void, MovieJsonObject> {
        @Override
        protected MovieJsonObject doInBackground(URL... params) {
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) params[0].openConnection();
                int response = connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder jsonBuilder = new StringBuilder();
                    try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                        String line;
                        while ((line = buffReader.readLine()) != null) {
                            jsonBuilder.append(line);
                        }
                    } catch (IOException e) {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.error_connection, Snackbar.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                    Gson gson = new GsonBuilder().create();
                    return gson.fromJson(jsonBuilder.toString(), MovieJsonObject.class);
                }
                return null;
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(MovieJsonObject movieJsonObject) {
            fillArrayList(movieJsonObject);
            moviesArrayAdapter.notifyDataSetChanged();
            listViewReviews.smoothScrollToPosition(0);
            progress.setVisibility(View.INVISIBLE);
        }
        void fillArrayList(MovieJsonObject movieJsonObject){
            moviesList.clear();

                if (movieJsonObject != null) {
                    if(movieJsonObject.getResults().size()>0) {
                        try {
                            for (Result result : movieJsonObject.getResults()) {
                                moviesList.add(new MovieListItem(getApplicationContext(),result));
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    else {
                        Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.movie_notfound, Snackbar.LENGTH_LONG).show();
                    }
                }
        }
    }
}
