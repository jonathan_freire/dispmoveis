package com.projetofatec.alexmantovanijonathanfreire;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.projetofatec.alexmantovanijonathanfreire.jsontemplate.Result;

import java.net.URLDecoder;
import java.net.URLEncoder;

public class ReviewActivity extends AppCompatActivity {
    ImageView image;
    TextView textView;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        Result movieresult = MainActivity.SelectedItem.getMovieResult();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(movieresult.getDisplayTitle());
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        image = (ImageView)findViewById(R.id.imageViewMovieReview);
        textView = (TextView)findViewById(R.id.textviewMovieReview);

        Intent intent = getIntent();
        Bitmap bitmap;
        if(!MainActivity.SelectedItem.getImageUrl().equals(getResources().getString(R.string.default_image_url))) {
            try {
                bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
                image.setImageBitmap(bitmap);
            } catch (Exception e) {

            }
        }

        String MovieText = movieresult.getSummaryShort();

        textView.setText( Html.fromHtml(MovieText ).toString());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
